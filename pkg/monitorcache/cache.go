package monitorcache

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/teuf/okz/pkg/okkazeo"
)

func getMonitorDir() (string, error) {
	homedir, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}
	return filepath.Join(homedir, ".local", "share", "okz", "monitor"), nil
}

func filenameForId(gameId int) (string, error) {
	dir, err := getMonitorDir()
	if err != nil {
		return "", err
	}

	return filepath.Join(dir, fmt.Sprintf("%d.json", gameId)), nil
}

func saveGame(game *okkazeo.Game) error {
	filename, err := filenameForId(game.Id)
	if err != nil {
		return err
	}
	err = os.MkdirAll(filepath.Dir(filename), 0755)
	if err != nil {
		return err
	}

	return game.WriteFile(filename)
}

func ListingDiff(localGame *okkazeo.Game) ([]*okkazeo.Item, []*okkazeo.Item, error) {
	okzGame, err := okkazeo.ParseGamePage(localGame.Id)
	if err != nil {
		return nil, nil, err
		//okzGame = okkazeo.NewGame(gameId)
	}
	if localGame.Name == "" {
		localGame.Name = okzGame.Name
	}

	var newListings []*okkazeo.Item
	var removedListings []*okkazeo.Item

	for _, item := range localGame.Items {
		if item.IsUnlisted() {
			continue
		}
		if !okzGame.HasListing(item.Id) {
			item.Unlist()
			removedListings = append(removedListings, item)
		}
	}

	for _, item := range okzGame.Items {
		if !localGame.HasListing(item.Id) {
			localGame.List(item)
			newListings = append(newListings, item)
		}
	}

	err = saveGame(localGame)
	if err != nil {
		/* log warning */
	}

	return newListings, removedListings, nil
}

func GetMonitoredGames() ([]*okkazeo.Game, error) {
	dir, err := getMonitorDir()
	if err != nil {
		return nil, err
	}
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	var games []*okkazeo.Game

	for _, f := range files {
		if f.Mode().IsRegular() {
			game, err := okkazeo.NewFromFile(filepath.Join(dir, f.Name()))
			if err != nil {
				log.Printf("Error reading %s\n", f.Name())
				continue
			}
			games = append(games, game)
		}
	}

	return games, nil
}

func MonitorStop(id int) {
	filename, _ := filenameForId(id)
	os.Remove(filename)
}

func IsMonitored(id int) bool {
	filename, err := filenameForId(id)
	if err != nil {
		return false
	}
	info, err := os.Stat(filename)
	if err != nil {
		return false
	}
	return info.Mode().IsRegular()
}
