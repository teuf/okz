/*
Copyright 2019, Christophe Fergeau <christophe@fergeau.eu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package okkazeo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/anaskhan96/soup"
)

type Item struct {
	Id           int       `json:"listingid"`
	Price        float32   `json:"price"`
	Seller       string    `json:"seller"`
	SellerId     int       `json:"sellerid"`
	City         string    `json:"city"`
	ZipCode      int       `json:"zipcode"`
	Country      string    `json:"country"`
	Shipping     string    `json:"shipping"`
	Condition    string    `json:"condition"`
	Version      string    `json:"version"`
	Description  string    `json:"description"`
	DateListed   time.Time `json:"listingdate"`
	DateUnlisted time.Time `json:"unlisteddate"`
	//GameId      int
}

type Game struct {
	Name  string        `json:"name"`
	Id    int           `json:"gameid"`
	Items map[int]*Item `json:"items"`
}

type ByDateListed []*Item

func (a ByDateListed) Len() int {
	return len(a)
}

func (a ByDateListed) Less(i, j int) bool {
	return a[j].DateListed.After(a[i].DateListed)
}

func (a ByDateListed) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]

}

type ByPrice []*Item

func (a ByPrice) Len() int {
	return len(a)
}

func (a ByPrice) Less(i, j int) bool {
	return a[i].Price < a[j].Price
}

func (a ByPrice) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]

}

func (item *Item) printWithPrefix(prefix string, verbose bool) {
	fmt.Printf("%s#%d:\n", prefix, item.Id)
	fmt.Printf("%sprice: %2.2f€\n", prefix, item.Price)
	fmt.Printf("%slocation: %d %s, %s\n", prefix, item.ZipCode, item.City, item.Country)
	date := item.DateListed.Format("2006-01-02")
	fmt.Printf("%slisted on %s by %s (#%d)\n", prefix, date, item.Seller, item.SellerId)
	if item.Version != "" {
		fmt.Printf("%sversion: %s\n", prefix, item.Version)
	}
	fmt.Printf("%scondition: %s\n", prefix, item.Condition)
	if item.Shipping != "" {
		fmt.Printf("%sshipping: %s\n", prefix, item.Shipping)
	}
	if verbose {
		fmt.Printf("%sdescription: %s\n", prefix, item.Description)
	}
}

func (game *Game) getListedItems() []*Item {
	var items []*Item
	for _, item := range game.Items {
		if item.IsUnlisted() {
			continue
		}
		items = append(items, item)
	}

	return items
}

func (game *Game) Print(sortByDate bool, verbose bool) {
	fmt.Printf("%s (#%d)\n", game.Name, game.Id)
	fmt.Printf("%d listings\n", len(game.Items))

	items := game.getListedItems()
	if sortByDate {
		sort.Sort(sort.Reverse(ByDateListed(items)))
	} else {
		sort.Sort(ByPrice(items))
	}

	for _, item := range items {
		item.printWithPrefix("\t", verbose)
		fmt.Println("")
	}
}

func printNode(node soup.Root) {
	if node.Pointer == nil {
		fmt.Printf("<empty node>\n")
		return
	}

	fmt.Printf("<%s> %s\n", node.NodeValue, node.Text())
	for attr, value := range node.Attrs() {
		fmt.Printf("\t%s: \"%s\"\n", attr, value)
	}
}

func nodeNotFound(node soup.Root) bool {
	return node.Pointer == nil
}

func (item *Item) parseId(div_small_4 soup.Root) error {
	url := div_small_4.Find("a")
	if nodeNotFound(url) {
		return fmt.Errorf("could not find <a> node holding item id information")
	}
	//log.Printf("url: %s\n", url.Attrs()["href"])

	idStr := strings.TrimPrefix(url.Attrs()["href"], "/annonces/view/")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Printf("Failed to parse %s: %s", idStr, err.Error())
		return err
	}
	item.Id = id

	return nil
}

func (item *Item) parsePrice(div_small_4 soup.Root) error {
	/* price */
	price := div_small_4.Find("div", "class", "prix")
	//printNode(price)
	if nodeNotFound(price) {
		return fmt.Errorf("could not find <div class='prix'> node holding price information")
	}
	var err error
	priceStr := strings.Replace(strings.TrimSuffix(price.Text(), " €"), ",", ".", -1)
	price64, err := strconv.ParseFloat(priceStr, 32)
	if err != nil {
		log.Printf("Failed to parse %s: %s", priceStr, err.Error())
		return err
	}
	item.Price = float32(price64)
	//log.Printf("price: %d\n", item.price)

	return nil
}

func (item *Item) parseSellerInfo(div_small_6 soup.Root) error {
	sellerNode := div_small_6.Find("a")
	if nodeNotFound(sellerNode) {
		return fmt.Errorf("could not find <a> node holding seller information")
	}

	sellerIdStr := strings.TrimPrefix(sellerNode.Attrs()["href"], "/users/viewProfil/")
	sellerId, err := strconv.Atoi(sellerIdStr)
	if err != nil {
		log.Printf("Failed to parse %s: %s", sellerIdStr, err.Error())
		return err
	}
	item.Seller = strings.TrimSpace(sellerNode.Text())
	item.SellerId = sellerId
	//log.Printf("seller: %s (%d)\n", item.seller, item.sellerId)

	return nil
}

func (item *Item) parseLocationInfo(div_small_6 soup.Root) error {
	img := div_small_6.Find("img")
	if nodeNotFound(img) {
		return fmt.Errorf("could not find <img> node holding seller location information")
	}
	city := strings.TrimSpace(img.FindPrevSibling().NodeValue)
	zipAndCountryStr := strings.TrimSpace(img.FindNextSibling().NodeValue)
	zipAndCountryStr = strings.TrimPrefix(zipAndCountryStr, "(")
	zipAndCountryStr = strings.TrimSuffix(zipAndCountryStr, ")")

	zipAndCountry := strings.SplitN(zipAndCountryStr, " ", 2)
	if len(zipAndCountry) != 2 {
		return fmt.Errorf("could not parse seller location information")
	}
	zipCode, err := strconv.Atoi(zipAndCountry[0])
	if err != nil {
		log.Printf("Failed to parse %s: %s", zipAndCountry[0], err.Error())
		return err
	}
	item.City = city
	item.ZipCode = zipCode
	item.Country = zipAndCountry[1]

	return nil
}

func (item *Item) parseItemDetails(article soup.Root) error {
	if item.Id == 0 {
		return fmt.Errorf("invalid item id %d", item.Id)
	}

	etat := article.Find("div", "id", fmt.Sprintf("etat-%d", item.Id))
	item.Condition = strings.TrimSpace(etat.Text())

	edition := article.Find("div", "id", fmt.Sprintf("edition-%d", item.Id))
	item.Version = strings.TrimSpace(edition.Text())

	description := article.Find("div", "class", "small-12").Find("p")
	item.Description = strings.TrimSpace(description.FullText())

	return nil
}

func (item *Item) parseDateListed(article soup.Root) error {
	if item.Id == 0 {
		return fmt.Errorf("invalid item id %d", item.Id)
	}
	dateListedNode := article.Find("div", "id", fmt.Sprintf("publication-%d", item.Id))
	dateListedStr := strings.TrimSpace(dateListedNode.Text())

	var err error
	item.DateListed, err = time.Parse("02/01/2006", dateListedStr)
	if err != nil {
		log.Printf("Failed to parse %s: %s", dateListedStr, err.Error())
		return err
	}

	return nil
}

func (item *Item) parseShipping(article soup.Root) error {
	if item.Id == 0 {
		return fmt.Errorf("invalid item id %d", item.Id)
	}

	shipping := article.Find("div", "id", fmt.Sprintf("livraison-%d", item.Id))
	item.Shipping = strings.TrimSpace(shipping.Text())

	return nil
}

func parseItem(article soup.Root) (*Item, error) {
	div := article.Find("div", "class", "small-4")
	if nodeNotFound(div) {
		log.Printf("invalid <article> node, could not find a <div> subnode with 'small-4' class")
		return nil, fmt.Errorf("failed to find item in <article> node")
	}
	item := Item{}

	err := item.parsePrice(div)
	if err != nil {
		log.Printf("failed to parse price: %s", err.Error())
		return nil, err
	}

	err = item.parseId(div)
	if err != nil {
		log.Printf("failed to parse item id: %s", err.Error())
		return nil, err
	}

	div = article.Find("div", "class", "small-6")
	//fmt.Printf("%v", div)
	if nodeNotFound(div) {
		log.Printf("invalid <article> node, could not find a <div> subnode with 'small-6' class")
		return nil, err
	}

	err = item.parseSellerInfo(div)
	if err != nil {
		log.Printf("failed to parse seller information, leaving it blank: %s", err.Error())
	}

	err = item.parseLocationInfo(div)
	if err != nil {
		log.Printf("failed to parse location information, leaving it blank: %s", err.Error())
	}

	err = item.parseItemDetails(article)
	if err != nil {
		log.Printf("failed to parse item details, leaving it blank: %s", err.Error())
	}

	err = item.parseShipping(article)
	if err != nil {
		log.Printf("failed to parse item date, leaving it blank: %s", err.Error())
	}

	err = item.parseDateListed(article)
	if err != nil {
		log.Printf("failed to parse item date, leaving it blank: %s", err.Error())
	}

	return &item, nil
}

func ParseGamePage(gameId int) (*Game, error) {
	url := fmt.Sprintf("https://www.okkazeo.com/jeux/view/%d", gameId)
	resp, err := soup.Get(url)

	if err != nil {
		return nil, err
	}
	doc := soup.HTMLParse(resp)
	gameName := strings.TrimSpace(doc.Find("h2").Text())
	if gameName == "" {
		return nil, fmt.Errorf("failed to parse game name")
	}
	game := NewGame(gameId)
	game.Name = gameName

	mbs := doc.Find("section", "class", "mbs")
	if nodeNotFound(mbs) {
		return game, nil
	}
	articles := mbs.FindAll("article")

	for _, article := range articles {
		item, err := parseItem(article)
		if err != nil {
			continue
		}
		if _, present := game.Items[item.Id]; present {
			log.Printf("Ignoring duplicate listing #%d", item.Id)
			continue
		}
		game.List(item)
	}

	return game, nil
}

func NewGame(gameId int) *Game {
	return &Game{Id: gameId, Items: make(map[int]*Item)}
}

func (game *Game) List(item *Item) {
	game.Items[item.Id] = item
}

func (game *Game) FindListing(id int) *Item {
	return game.Items[id]
}

func (game *Game) HasListing(id int) bool {
	_, found := game.Items[id]

	return found
}

func (game *Game) WriteFile(filename string) error {
	data, err := json.Marshal(game)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func NewFromFile(filename string) (*Game, error) {
	var game Game

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &game)
	if err != nil {
		return nil, err
	}

	return &game, nil
}

func (item *Item) IsUnlisted() bool {
	return item.DateUnlisted.After(item.DateListed)
}

func (item *Item) Unlist() {
	item.DateUnlisted = time.Now()
}
