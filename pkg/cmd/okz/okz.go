/*
Copyright 2019, Christophe Fergeau <christophe@fergeau.eu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package okz

import (
	"github.com/spf13/cobra"
	"gitlab.com/teuf/okz/pkg/cmd/cli/list-ads"
	"gitlab.com/teuf/okz/pkg/cmd/cli/monitor"
)

func NewCommand(name string) *cobra.Command {
	c := &cobra.Command{
		Use:   name,
		Short: "Commandline interface to the Okkazeo website",
		Long:  `okz is a tool to list and monitor ads on the French used board games website Okkazeo`,
	}

	c.AddCommand(listads.NewCommand())
	c.AddCommand(monitor.NewCommand())

	return c
}
