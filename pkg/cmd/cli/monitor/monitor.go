/*
Copyright 2019, Christophe Fergeau <christophe@fergeau.eu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package monitor

import (
	"fmt"
	"io"
	"os"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/teuf/okz/pkg/cmd/cli/common"
	"gitlab.com/teuf/okz/pkg/monitorcache"
	"gitlab.com/teuf/okz/pkg/okkazeo"
)

func NewCommand() *cobra.Command {
	monitorCmd := &cobra.Command{
		Use:   "monitor",
		Short: "Manage games which are monitored for new ads",
		Run: func(c *cobra.Command, args []string) {
			printMonitor(os.Stdout, "monitor")
		},
	}

	var details bool
	var sold bool
	listCmd := &cobra.Command{
		Use:   "list",
		Short: "List the games which are currently monitored",
		Args:  cobra.NoArgs,
		Run: func(c *cobra.Command, args []string) {
			listMonitoredGames(details, sold)
		},
	}
	listCmd.Flags().BoolVarP(&details, "details", "d", false, "Detailed monitored games listing")
	listCmd.Flags().BoolVarP(&sold, "sold", "s", false, "If showing details, also show the sold games")

	updateCmd := &cobra.Command{
		Use:   "update",
		Short: "Update the known ads for the monitored games",
		Args:  cobra.NoArgs,
		Run: func(c *cobra.Command, args []string) {
			updateMonitorState()
		},
	}

	addCmd := &cobra.Command{
		Use:   "add",
		Short: "Add new game(s) to monitor",
		Args:  common.GameIdArgs,
		Run: func(c *cobra.Command, args []string) {
			ids, err := common.ParseGameIds(args)
			if err != nil {
				/* This should not happen as the Args validator
				   is checking this */
				os.Exit(1)
			}
			addMonitoredGames(ids)
		},
	}

	removeCmd := &cobra.Command{
		Use:   "remove",
		Short: "Remove game(s) from monitor",
		Args:  common.GameIdArgs,
		Run: func(c *cobra.Command, args []string) {
			ids, err := common.ParseGameIds(args)
			if err != nil {
				/* This should not happen as the Args validator
				   is checking this */
				os.Exit(1)
			}
			removeMonitoredGames(ids)
		},
	}

	monitorCmd.AddCommand(listCmd)
	monitorCmd.AddCommand(updateCmd)
	monitorCmd.AddCommand(addCmd)
	monitorCmd.AddCommand(removeCmd)

	return monitorCmd
}

func printMonitor(w io.Writer, cmd string) {
	fmt.Println(cmd)
}

func sortByPrice(items []*okkazeo.Item) []*okkazeo.Item {
	sort.Slice(items, func(i, j int) bool {
		return items[i].Price < items[j].Price
	})

	return items
}

func printAds(game *okkazeo.Game, showSold bool) {
	items := []*okkazeo.Item{}
	for _, item := range game.Items {
		items = append(items, item)
	}
	sortByPrice(items)
	for _, item := range items {
		if !item.IsUnlisted() {
			fmt.Printf("\t%.02f € - put on sale by %s in %s (%d), %s\n", item.Price, item.Seller, item.City, item.ZipCode, item.Country)
		} else if showSold {
			fmt.Printf("\t%.02f € - sold on %s by %s\n", item.Price, item.DateUnlisted.Format("2006-01-02"), item.Seller)
		}
	}
	fmt.Println("")
}

func listMonitoredGames(verbose bool, showSold bool) {
	games, err := monitorcache.GetMonitoredGames()
	if err != nil {
		fmt.Printf("Error reading monitored games cache\n")
		fmt.Printf("debug: %v", err)
		os.Exit(1)
	}
	sort.Slice(games, func(i, j int) bool {
		return games[i].Name < games[j].Name
	})

	fmt.Printf("Monitored games:\n\n")
	for _, game := range games {
		fmt.Printf("%s (#%d)\n", game.Name, game.Id)
		if verbose {
			printAds(game, showSold)
		}
	}
}

func removeMonitoredGames(gameIds []int) {
	for _, id := range gameIds {
		monitorcache.MonitorStop(id)
	}
}

func addMonitoredGames(gameIds []int) {
	for _, id := range gameIds {
		if monitorcache.IsMonitored(id) {
			fmt.Printf("skipping #%d as it is already monitored\n", id)
			continue
		}
		updateListing(okkazeo.NewGame(id))
	}
}

func updateListing(game *okkazeo.Game) error {
	added, removed, err := monitorcache.ListingDiff(game)
	if err != nil {
		fmt.Printf("Failed to get updated listings for %s (#%d): %v\n",
			game.Name, game.Id, err)
		return err
	}
	if len(removed) == 0 && len(added) == 0 {
		return nil
	}
	fmt.Printf("%s (#%d)\n", game.Name, game.Id)
	for _, item := range sortByPrice(removed) {
		fmt.Printf("\tsold for %.2f€ by %s\n", item.Price, item.Seller)
	}
	for _, item := range sortByPrice(added) {
		fmt.Printf("\tnew item for sale: %.2f€ from %s\n", item.Price, item.Seller)
	}

	return nil
}

func updateMonitorState() {
	games, err := monitorcache.GetMonitoredGames()
	if err != nil {
		fmt.Printf("Error reading monitored games cache\n")
		fmt.Printf("debug: %v", err)
		os.Exit(1)
	}

	for _, game := range games {
		updateListing(game)
	}
}
