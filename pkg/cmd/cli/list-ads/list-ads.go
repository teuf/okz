/*
Copyright 2019, Christophe Fergeau <christophe@fergeau.eu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package listads

import (
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/teuf/okz/pkg/cmd/cli/common"
	"gitlab.com/teuf/okz/pkg/okkazeo"
)

func NewCommand() *cobra.Command {
	var sortByDate bool

	c := &cobra.Command{
		Use:   "list-ads",
		Short: "Print the ads for game(s)",
		Args:  common.GameIdArgs,
		Run: func(c *cobra.Command, args []string) {
			ids, err := common.ParseGameIds(args)
			if err != nil {
				/* This should not happen as the Args validator
				   is checking this */
				os.Exit(1)
			}
			printAds(os.Stdout, ids, sortByDate)
		},
	}

	c.Flags().BoolVarP(&sortByDate, "sort-by-date", "d", false, "sort by listing date instead of price")

	return c
}

func printAds(w io.Writer, ids []int, sortByDate bool) {
	for _, id := range ids {
		game, err := okkazeo.ParseGamePage(id)
		if err != nil {
			continue
			//os.Exit(1)
		}
		game.Print(sortByDate, false)
	}

}
