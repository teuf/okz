package common

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

func ParseGameIds(args []string) ([]int, error) {
	var ids []int

	for _, arg := range args {
		id, err := strconv.Atoi(arg)
		if err != nil {
			return nil, fmt.Errorf("invalid id [%s], this should be an integer value", arg)
		}
		ids = append(ids, id)
	}

	return ids, nil
}

func GameIdArgs(cmd *cobra.Command, args []string) error {
	if len(args) < 1 {
		return errors.New("requires at least one game id")
	}
	_, err := ParseGameIds(args)
	if err != nil {
		return err
	}

	return nil
}
