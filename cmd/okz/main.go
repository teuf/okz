/*
Copyright 2019, Christophe Fergeau <christophe@fergeau.eu>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"os"
	"path/filepath"

	"gitlab.com/teuf/okz/pkg/cmd/okz"
)

func main() {
	baseName := filepath.Base(os.Args[0])
	err := okz.NewCommand(baseName).Execute()
	if err != nil {
		os.Exit(1)
	}
}
