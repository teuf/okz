module gitlab.com/teuf/okz

go 1.12

require (
	github.com/anaskhan96/soup v1.1.2-0.20190517160040-b32d4fb0aaab
	github.com/spf13/cobra v0.0.5
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
)
